# Declarative management of etcd auth configuration

`etcdauthctl` is an experimental tool for declarative management of etcd v3 auth
configuration.

**Note**: This is a pre-release proof-of-concept, and should not be used for
anything at this time.

## Commands

```
etcdauthctl -config=CONFIG show
etcdauthctl -config=CONFIG diff INTENT
etcdauthctl -config=CONFIG apply INTENT [-confirm=true|false -sync=true|false]
```

where `CONFIG` and `INTENT` are YAML files described below.

* `show` - Collect live auth configuration and emit its YAML representation to
  stdout (minus passwords, which cannot be retrieved from the API).
* `diff` - Collect live auth configuration and emit the difference between it
  and the provided configuration intent to stdout (as YAML).
* `apply` - Collect live auth configuration, emit the difference between it
  and the provided configuration intent to stdout, optionally await user
  confirmation, and update the live configuration to match intent.

The `apply` command can also be used to synchronize all user passwords to match
intent (`-sync=true`). In addition, interactive confirmation can be skipped by
specifying `-confirm=false`.

Both the `apply` and `diff` commands accept a `-context` flag, which sets the
number of context lines used when displaying diffs.

## Auth intent

Example: `user0` has read permission over the entire key prefix `/foo`

```
roles:
  role0:
    permissions:
      /foo:
        prefix: true
        perm: READ
  root:
    permissions: {}
users:
  root:
    password: a_great_password
    roles:
      - root
  user0:
    password: an_also_great_password
    roles:
      - role0
```

For simplicity and clarity, permissions for key ranges are only supported in the
prefix case - i.e., we support single keys or prefixes rooted at those keys, but
not arbitrary ranges.

Further, permissions are 1:1 with keys in a given role, meaning that a role can
have a permission defined for either a single key _or_ the prefix rooted at that
key (but not both).

## Client configuration

Example:

```
endpoints:
  - 127.0.0.1:2379
username: root
password: a_great_password
ca_cert: /etc/path/to/a/cert.pem
```

## TODO

* Consider adopting zap over slog for consistency with the etcd client.
* Consider supporting endpoint discovery from DNS SRV records.
* Consider adding an integration test against a local etcd instance.
