package api

import (
	"context"
	"fmt"

	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
)

// AuthClient is a subset of the clientv3.Auth API.
type AuthClient interface {
	RoleList(ctx context.Context) (*clientv3.AuthRoleListResponse, error)
	RoleGet(ctx context.Context, role string) (*clientv3.AuthRoleGetResponse, error)
	UserList(ctx context.Context) (*clientv3.AuthUserListResponse, error)
	UserGet(ctx context.Context, name string) (*clientv3.AuthUserGetResponse, error)
}

// Collect uses the provided auth API client to collect user and role
// configuration in config.Auth form.
func Collect(ctx context.Context, auth clientv3.Auth) (*config.Auth, error) {
	rl, err := auth.RoleList(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to query live roles: %v", err)
	}
	ac := &config.Auth{
		Roles: make(map[string]*config.Role),
		Users: make(map[string]*config.User),
	}
	for _, role := range rl.Roles {
		rg, err := auth.RoleGet(ctx, role)
		if err != nil {
			return nil, fmt.Errorf("failed to query live configuration for role %q: %v", role, err)
		}
		r := &config.Role{Permissions: make(map[string]*config.Permission)}
		for _, perm := range rg.Perm {
			p := new(config.Permission)
			k := string(perm.Key)
			switch perm.PermType {
			case clientv3.PermRead:
				p.Perm = config.PermRead
			case clientv3.PermWrite:
				p.Perm = config.PermWrite
			case clientv3.PermReadWrite:
				p.Perm = config.PermReadWrite
			default:
				return nil, fmt.Errorf("unrecognized permission type in live configuration for role %q key %q: %v", role, k, err)
			}
			if len(perm.RangeEnd) != 0 {
				if clientv3.GetPrefixRangeEnd(string(perm.Key)) == string(perm.RangeEnd) {
					p.Prefix = true
				} else {
					return nil, fmt.Errorf("unsupported permission in live configuration for role %q: key %q has non-prefix range-end", role, k)
				}
			}
			if _, ok := r.Permissions[k]; ok {
				return nil, fmt.Errorf("unsupported permission in live configuration for role %q: multiple permissions for key %q", role, k)
			}
			r.Permissions[string(perm.Key)] = p
		}
		ac.Roles[role] = r
	}
	ul, err := auth.UserList(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to query live users: %v", err)
	}
	for _, user := range ul.Users {
		ug, err := auth.UserGet(ctx, user)
		if err != nil {
			return nil, fmt.Errorf("failed to query live configuration for user %q: %v", user, err)
		}
		ac.Users[user] = &config.User{Roles: ug.Roles}
	}
	return ac, nil
}
