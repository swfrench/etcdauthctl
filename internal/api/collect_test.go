package api_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
	"go.etcd.io/etcd/api/v3/authpb"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/api"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
)

type fakeAuthClient struct {
	clientv3.Auth // Note: only a narrow subset of methods are implemented
	roles         map[string]*clientv3.AuthRoleGetResponse
	users         map[string]*clientv3.AuthUserGetResponse
}

func (fac *fakeAuthClient) RoleList(ctx context.Context) (*clientv3.AuthRoleListResponse, error) {
	if len(fac.roles) == 0 {
		return nil, fmt.Errorf("no roles available")
	}
	r := &clientv3.AuthRoleListResponse{}
	for role := range fac.roles {
		r.Roles = append(r.Roles, role)
	}
	return r, nil
}

func (fac *fakeAuthClient) RoleGet(ctx context.Context, role string) (*clientv3.AuthRoleGetResponse, error) {
	r := fac.roles[role]
	if r == nil {
		return nil, fmt.Errorf("role %s not found", role)
	}
	return r, nil
}

func (fac *fakeAuthClient) UserList(ctx context.Context) (*clientv3.AuthUserListResponse, error) {
	if len(fac.users) == 0 {
		return nil, fmt.Errorf("no users available")
	}
	r := &clientv3.AuthUserListResponse{}
	for user := range fac.users {
		r.Users = append(r.Users, user)
	}
	return r, nil
}

func (fac *fakeAuthClient) UserGet(ctx context.Context, name string) (*clientv3.AuthUserGetResponse, error) {
	r := fac.users[name]
	if r == nil {
		return nil, fmt.Errorf("user %s not found", name)
	}
	return r, nil
}

func TestCollect(t *testing.T) {
	testCases := []struct {
		name    string
		fac     *fakeAuthClient
		want    *config.Auth
		wantErr bool
	}{
		{
			name: "success",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": {},
					"foo-reader-role": {
						Perm: []*authpb.Permission{
							{Key: []byte("/foo"), PermType: authpb.READ},
						},
					},
					"bar-prefix-writer-role": {
						Perm: []*authpb.Permission{
							{Key: []byte("/bar"), RangeEnd: []byte("/bas"), PermType: authpb.WRITE},
						},
					},
					"baz-reader-writer-role": {
						Perm: []*authpb.Permission{
							{Key: []byte("/baz"), PermType: authpb.READWRITE},
						},
					},
				},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root":   {Roles: []string{"root"}},
					"a-user": {Roles: []string{"foo-reader-role", "bar-prefix-writer-role", "baz-reader-writer-role"}},
				},
			},
			want: &config.Auth{
				Roles: map[string]*config.Role{
					"root": {
						Permissions: map[string]*config.Permission{},
					},
					"foo-reader-role": {
						Permissions: map[string]*config.Permission{
							"/foo": {Prefix: false, Perm: config.PermRead},
						},
					},
					"bar-prefix-writer-role": {
						Permissions: map[string]*config.Permission{
							"/bar": {Prefix: true, Perm: config.PermWrite},
						},
					},
					"baz-reader-writer-role": {
						Permissions: map[string]*config.Permission{
							"/baz": {Perm: config.PermReadWrite},
						},
					},
				},
				Users: map[string]*config.User{
					"root":   {Roles: []string{"root"}},
					"a-user": {Roles: []string{"foo-reader-role", "bar-prefix-writer-role", "baz-reader-writer-role"}},
				},
			},
		},
		{
			name: "fails in list roles",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root": {Roles: []string{"root"}},
				},
			},
			wantErr: true,
		},
		{
			name: "fails in get role",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": nil,
				},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root": {Roles: []string{"root"}},
				},
			},
			wantErr: true,
		},
		{
			name: "fails in list users",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": {},
				},
				users: map[string]*clientv3.AuthUserGetResponse{},
			},
			wantErr: true,
		},
		{
			name: "fails in get user",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": {},
				},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root": nil,
				},
			},
			wantErr: true,
		},
		{
			name: "fails on unrecognized permission type",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": {},
					"foo-bad-permission-type-role": {
						Perm: []*authpb.Permission{
							{Key: []byte("/foo"), PermType: authpb.Permission_Type(42)},
						},
					},
				},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root":   {Roles: []string{"root"}},
					"a-user": {Roles: []string{"foo-bad-permission-type-role"}},
				},
			},
			wantErr: true,
		},
		{
			name: "fails on unsupported multiple permissions for key",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": {},
					"foo-duplicate-role": {
						Perm: []*authpb.Permission{
							{Key: []byte("/foo"), PermType: authpb.READ},
							{Key: []byte("/foo"), RangeEnd: []byte("/fop"), PermType: authpb.READWRITE},
						},
					},
				},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root":   {Roles: []string{"root"}},
					"a-user": {Roles: []string{"foo-duplicate-role"}},
				},
			},
			wantErr: true,
		},
		{
			name: "fails on unsupported non-prefix range permissions",
			fac: &fakeAuthClient{
				roles: map[string]*clientv3.AuthRoleGetResponse{
					"root": {},
					"foo-non-prefix-range-role": {
						Perm: []*authpb.Permission{
							{Key: []byte("/foo"), RangeEnd: []byte("/foz"), PermType: authpb.READ},
						},
					},
				},
				users: map[string]*clientv3.AuthUserGetResponse{
					"root":   {Roles: []string{"root"}},
					"a-user": {Roles: []string{"foo-non-prefix-range-role"}},
				},
			},
			wantErr: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			auth, err := api.Collect(context.Background(), tc.fac)
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("Collect() returned incorrect error respose: gotErr: %t wantErr: %t err: %v", gotErr, tc.wantErr, err)
			}
			if tc.wantErr {
				return
			}
			if diff := cmp.Diff(tc.want, auth); diff != "" {
				t.Errorf("Collect() returned incorrect Auth config (+got,-want):\n%s", diff)
			}
		})
	}
}
