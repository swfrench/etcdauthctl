package commands

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"github.com/google/subcommands"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/api"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
	"wikimedia.org/operations/software/etcdauthctl/internal/ops"
)

type Apply struct {
	confirm       bool
	syncPasswords bool
	context       int
}

func ApplyCommand() *Apply { return new(Apply) }

func (*Apply) Name() string { return "apply" }
func (*Apply) Synopsis() string {
	return "update live auth configuration to match intent, after displaying the difference and (optionally) awaiting confirmation"
}
func (*Apply) Usage() string {
	return "Usage: etcdauthctl -config=CONFIG apply [-context=N -confirm=true|false -sync=true|false] INTENT\n"
}

func (a *Apply) SetFlags(f *flag.FlagSet) {
	f.BoolVar(&a.confirm, "confirm", true, "Whether to apply auth difference only after confirmation.")
	f.BoolVar(&a.syncPasswords, "sync", false, "Whether to reset all user passwords to their intent values.")
	f.IntVar(&a.context, "context", 5, "The number of lines of context to include in diffs.")
}

func confirm(prompt string) (bool, error) {
	r := bufio.NewReader(os.Stdin)
	fmt.Printf("%s [y/n]: ", prompt)
	raw, err := r.ReadString('\n')
	if err != nil {
		return false, fmt.Errorf("failed to read user response: %v", err)
	}
	if resp := strings.TrimSpace(raw); resp == "y" {
		return true, nil
	} else if resp == "n" {
		return false, nil
	} else {
		return false, fmt.Errorf("unexpected user response: %q", resp)
	}
}

func (a *Apply) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		slog.Error("Missing auth intent")
		return subcommands.ExitFailure
	}
	intent, err := config.LoadAuth(f.Arg(0))
	if err != nil {
		slog.Error("Failed to load auth intent", "error", err)
		return subcommands.ExitFailure
	}
	auth := args[0].(clientv3.Auth)
	live, err := api.Collect(ctx, auth)
	if err != nil {
		slog.Error("Failed to collect live auth configuration", "error", err)
		return subcommands.ExitFailure
	}
	diff, err := ops.Diff(live, intent, a.context, os.Stdout)
	if err != nil {
		slog.Error("Auth configuration diff failed", "error", err)
		return subcommands.ExitFailure
	}
	if !diff && !a.syncPasswords {
		slog.Info("No changes to apply")
		return subcommands.ExitSuccess
	}
	if a.confirm {
		var prompt string
		if a.syncPasswords {
			prompt = "Proceed apply changes and sync all user passwords?"
		} else {
			prompt = "Proceed apply changes?"
		}
		if proceed, err := confirm(prompt); err != nil {
			slog.Error("User confirmation failed", "error", err)
			return subcommands.ExitFailure
		} else if !proceed {
			slog.Error("Apply aborted by user")
			return subcommands.ExitFailure
		}
	}
	if err := ops.Apply(ctx, auth, live, intent, a.syncPasswords); err != nil {
		slog.Error("Auth configuration update failed", "error", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}
