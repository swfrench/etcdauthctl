package commands

import (
	"context"
	"flag"
	"log/slog"
	"os"

	"github.com/google/subcommands"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/api"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
	"wikimedia.org/operations/software/etcdauthctl/internal/ops"
)

type Diff struct {
	context int
}

func DiffCommand() *Diff { return new(Diff) }

func (*Diff) Name() string { return "diff" }
func (*Diff) Synopsis() string {
	return "fetch the live auth configuration and display the difference between it and the provided auth intent to stdout."
}
func (*Diff) Usage() string { return "Usage: etcdauthctl -config=CONFIG diff [-context=N] INTENT\n" }

func (d *Diff) SetFlags(f *flag.FlagSet) {
	f.IntVar(&d.context, "context", 5, "The number of lines of context to include in diffs.")
}

func (d *Diff) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		slog.Error("Missing auth intent")
		return subcommands.ExitFailure
	}
	intent, err := config.LoadAuth(f.Arg(0))
	if err != nil {
		slog.Error("Failed to load auth intent", "error", err)
		return subcommands.ExitFailure
	}
	auth := args[0].(clientv3.Auth)
	live, err := api.Collect(ctx, auth)
	if err != nil {
		slog.Error("Failed to collect live auth configuration", "error", err)
		return subcommands.ExitFailure
	}
	if _, err := ops.Diff(live, intent, d.context, os.Stdout); err != nil {
		slog.Error("Auth configuration diff failed", "error", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}
