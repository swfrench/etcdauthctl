package commands

import (
	"context"
	"flag"
	"fmt"
	"log/slog"

	"github.com/google/subcommands"
	clientv3 "go.etcd.io/etcd/client/v3"
	"gopkg.in/yaml.v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/api"
)

type Show struct{}

func ShowCommand() *Show { return new(Show) }

func (*Show) Name() string     { return "show" }
func (*Show) Synopsis() string { return "fetch and print the live auth configuration to stdout." }
func (*Show) Usage() string    { return "Usage: etcdauthctl -config CONFIG show\n" }

func (*Show) SetFlags(f *flag.FlagSet) {}

func (*Show) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	auth := args[0].(clientv3.Auth)
	ac, err := api.Collect(ctx, auth)
	if err != nil {
		slog.Error("Failed to collect live auth configuration", "error", err)
		return subcommands.ExitFailure
	}
	yb, err := yaml.Marshal(ac.Redacted())
	if err != nil {
		slog.Error("Failed to marshal live auth configuration", "error", err)
		return subcommands.ExitFailure
	}
	fmt.Printf("Live configuration:\n===\n\n%s\n===\n", string(yb))
	return subcommands.ExitSuccess
}
