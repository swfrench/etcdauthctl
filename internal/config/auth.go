package config

import (
	"errors"
	"fmt"
	"os"
	"slices"

	clientv3 "go.etcd.io/etcd/client/v3"
	"gopkg.in/yaml.v3"
)

type Perm int

const (
	PermUnknown   Perm = iota
	PermRead      Perm = iota
	PermWrite     Perm = iota
	PermReadWrite Perm = iota
)

func (p Perm) MarshalYAML() (interface{}, error) {
	switch p {
	case PermRead:
		return "READ", nil
	case PermWrite:
		return "WRITE", nil
	case PermReadWrite:
		return "READWRITE", nil
	default:
		return "", fmt.Errorf("cannot marshal Perm with value %v", p)
	}
}

func (p *Perm) UnmarshalYAML(n *yaml.Node) error {
	switch n.Value {
	case "READ":
		*p = PermRead
	case "WRITE":
		*p = PermWrite
	case "READWRITE":
		*p = PermReadWrite
	default:
		return fmt.Errorf("cannot marshal Perm with value %v", p)
	}
	return nil
}

func (p Perm) ToProto() (clientv3.PermissionType, error) {
	switch p {
	case PermRead:
		return clientv3.PermissionType(clientv3.PermRead), nil
	case PermWrite:
		return clientv3.PermissionType(clientv3.PermWrite), nil
	case PermReadWrite:
		return clientv3.PermissionType(clientv3.PermReadWrite), nil
	default:
		return 0, fmt.Errorf("cannot convert Perm with value %v to proto", p)
	}
}

type Permission struct {
	Prefix bool `yaml:"prefix"`
	Perm   Perm `yaml:"perm"`
}

type Role struct {
	Permissions map[string]*Permission `yaml:"permissions"`
}

type User struct {
	Password string   `yaml:"password"`
	Roles    []string `yaml:"roles"`
}

type Auth struct {
	Roles map[string]*Role `yaml:"roles"`
	Users map[string]*User `yaml:"users"`
}

const redactedPassword = "********"

func (a *Auth) Redacted() *Auth {
	r := &Auth{
		Roles: a.Roles,
		Users: make(map[string]*User),
	}
	for n, u := range a.Users {
		r.Users[n] = &User{
			Password: redactedPassword,
			Roles:    u.Roles,
		}
	}
	return r
}

func LoadAuth(filename string) (*Auth, error) {
	bs, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to read auth config: %v", err)
	}
	a := new(Auth)
	if err := yaml.Unmarshal(bs, a); err != nil {
		return nil, fmt.Errorf("failed to unmarshal auth config: %v", err)
	}
	var errs []error
	var rootUser bool
	for n, u := range a.Users {
		for _, r := range u.Roles {
			if _, ok := a.Roles[r]; !ok {
				errs = append(errs, fmt.Errorf("user %s references unknown role %s", n, r))
			}
		}
		if u.Password == "" {
			errs = append(errs, fmt.Errorf("no password specified for user %s", n))
		}
		if n == "root" {
			rootUser = true
			if !slices.Contains(u.Roles, "root") {
				errs = append(errs, fmt.Errorf("root user is not granted root role"))
			}
		}
	}
	if !rootUser {
		errs = append(errs, fmt.Errorf("root user missing from config"))
	}
	for n, r := range a.Roles {
		for k, p := range r.Permissions {
			if p.Perm == PermUnknown {
				errs = append(errs, fmt.Errorf("no perm specified for role %s key %s", n, k))
			}
		}
	}
	if len(errs) > 0 {
		return nil, errors.Join(errs...)
	}
	return a, nil
}
