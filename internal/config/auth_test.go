package config_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
)

func TestLoadAuth(t *testing.T) {
	testCases := []struct {
		name     string
		filename string
		want     *config.Auth
		wantErr  bool
	}{
		{
			name:     "succeeds",
			filename: "testdata/auth_good.yaml",
			want: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermRead},
						},
					},
					"bar-prefix-writer": {
						Permissions: map[string]*config.Permission{
							"/bar": {Perm: config.PermWrite, Prefix: true},
						},
					},
					"baz-reader-writer": {
						Permissions: map[string]*config.Permission{
							"/baz": {Perm: config.PermReadWrite},
						},
					},
					"root": {},
				},
				Users: map[string]*config.User{
					"root":   {Roles: []string{"root"}, Password: "test-root"},
					"a-user": {Roles: []string{"foo-reader", "bar-prefix-writer", "baz-reader-writer"}, Password: "test-a-user"},
				},
			},
		},
		{
			name:     "fails when file does not exist",
			filename: "testdata/DOES_NOT_EXIST",
			wantErr:  true,
		},
		{
			name:     "fails when file is not valid yaml",
			filename: "testdata/auth_bad_not_yaml.yaml",
			wantErr:  true,
		},
		{
			name:     "fails when no root user is configured",
			filename: "testdata/auth_bad_no_root.yaml",
			wantErr:  true,
		},
		{
			name:     "fails when root user is not granted root role",
			filename: "testdata/auth_bad_root_is_not_root.yaml",
			wantErr:  true,
		},
		{
			name:     "fails when role granted unnown permission",
			filename: "testdata/auth_bad_invalid_perm.yaml",
			wantErr:  true,
		},
		{
			name:     "fails when role permission is missing",
			filename: "testdata/auth_bad_missing_perm.yaml",
			wantErr:  true,
		},
		{
			name:     "fails when user granted unknown role",
			filename: "testdata/auth_bad_unknown_role.yaml",
			wantErr:  true,
		},
		{
			name:     "fails when user has no password",
			filename: "testdata/auth_bad_missing_password.yaml",
			wantErr:  true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := config.LoadAuth(tc.filename)
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("LoadAuth() returned incorrect error status: gotErr: %t wantErr: %t err: %v", gotErr, tc.wantErr, err)
			}
			if tc.wantErr {
				return
			}
			if diff := cmp.Diff(tc.want, got); diff != "" {
				t.Errorf("LoadAuth() returned incorrect Auth config (+got,-want):\n%s", diff)
			}
		})
	}

}

func TestAuthRedacted(t *testing.T) {
	want := &config.Auth{
		Roles: map[string]*config.Role{
			"foo-reader": {
				Permissions: map[string]*config.Permission{
					"/foo": {Perm: config.PermRead},
				},
			},
			"root": {},
		},
		Users: map[string]*config.User{
			"root":   {Roles: []string{"root"}, Password: "********"},
			"a-user": {Roles: []string{"foo-reader"}, Password: "********"},
		},
	}
	a := &config.Auth{
		Roles: map[string]*config.Role{
			"foo-reader": {
				Permissions: map[string]*config.Permission{
					"/foo": {Perm: config.PermRead},
				},
			},
			"root": {},
		},
		Users: map[string]*config.User{
			"root":   {Roles: []string{"root"}, Password: "test-root"},
			"a-user": {Roles: []string{"foo-reader"}, Password: "test-a-user"},
		},
	}
	got := a.Redacted()
	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("Redacted() returned incorrect Auth config (+got,-want):\n%s", diff)
	}

}

func TestPermToProto(t *testing.T) {
	testCases := []struct {
		name    string
		perm    config.Perm
		want    clientv3.PermissionType
		wantErr bool
	}{
		{
			name: "succeeds READ",
			perm: config.PermRead,
			want: clientv3.PermissionType(clientv3.PermRead),
		},
		{
			name: "succeeds WRITE",
			perm: config.PermWrite,
			want: clientv3.PermissionType(clientv3.PermWrite),
		},
		{
			name: "succeeds READWRITE",
			perm: config.PermReadWrite,
			want: clientv3.PermissionType(clientv3.PermReadWrite),
		},
		{
			name:    "fails UNKNOWN",
			perm:    config.PermUnknown,
			wantErr: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.perm.ToProto()
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("ToProto() returned incorrect error status: gotErr: %t wantErr: %t err: %v", gotErr, tc.wantErr, err)
			}
			if tc.wantErr {
				return
			}
			if got != tc.want {
				t.Errorf("ToProto() returned incorrect PermissionType: got: %v want: %v", got, tc.want)
			}
		})
	}
}

func TestPermMarshal(t *testing.T) {
	testCases := []struct {
		name    string
		perm    config.Perm
		want    string
		wantErr bool
	}{
		{
			name: "succeeds READ",
			perm: config.PermRead,
			want: "READ",
		},
		{
			name: "succeeds WRITE",
			perm: config.PermWrite,
			want: "WRITE",
		},
		{
			name: "succeeds READWRITE",
			perm: config.PermReadWrite,
			want: "READWRITE",
		},
		{
			name:    "fails UNKNOWN",
			perm:    config.PermUnknown,
			wantErr: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got, err := tc.perm.MarshalYAML()
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("MarshalYAML() returned incorrect error status: gotErr: %t wantErr: %t err: %v", gotErr, tc.wantErr, err)
			}
			if tc.wantErr {
				return
			}
			if got != tc.want {
				t.Errorf("MarshalYAML() returned incorrect PermissionType: got: %v want: %v", got, tc.want)
			}
		})
	}
}
