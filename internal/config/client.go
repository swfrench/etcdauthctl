package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

// Client represents etcd client configuration.
type Client struct {
	// List of etcd node endpoints.
	Endpoints []string `yaml:"endpoints"`
	// Username to use in authenticating to etcd.
	Username string `yaml:"username"`
	// Password to use in authenticating to etcd.
	Password string `yaml:"password"`
	// Optional root CA certificates to use when verifying certificates
	// presented by etcd nodes.
	CACert string `yaml:"ca_cert"`
}

func LoadClient(filename string) (*Client, error) {
	bs, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to read client config: %v", err)
	}
	c := new(Client)
	if err := yaml.Unmarshal(bs, c); err != nil {
		return nil, fmt.Errorf("failed to unmarshal client config: %v", err)
	}
	return c, err
}
