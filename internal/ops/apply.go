package ops

import (
	"context"
	"fmt"
	"slices"

	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
)

type roleDiff struct {
	permsAdded map[string]any
	// NOTE: the value of permsChanged indicates whether the previous perm for
	// that key needs revoked. This happens in the case where we go between
	// prefix and non-prefix perms (which cannot be patched with a grant).
	permsChanged map[string]bool
	permsRemoved map[string]any
}

type rolesDiff struct {
	added   map[string]any
	changed map[string]*roleDiff
	removed map[string]any
}

func roleDifference(live, intent *config.Role) *roleDiff {
	rd := &roleDiff{
		permsAdded:   make(map[string]any),
		permsChanged: make(map[string]bool),
		permsRemoved: make(map[string]any),
	}
	for k := range live.Permissions {
		rd.permsRemoved[k] = nil
	}
	for k, ip := range intent.Permissions {
		if lp, ok := live.Permissions[k]; ok {
			if ip.Perm != lp.Perm || ip.Prefix != lp.Prefix {
				rd.permsChanged[k] = ip.Prefix != lp.Prefix
			}
			delete(rd.permsRemoved, k)
		} else {
			rd.permsAdded[k] = nil
		}
	}
	if len(rd.permsAdded) == 0 && len(rd.permsRemoved) == 0 && len(rd.permsChanged) == 0 {
		return nil
	}
	return rd
}

func rolesDifference(live, intent *config.Auth) *rolesDiff {
	rsd := &rolesDiff{
		added:   make(map[string]any),
		changed: make(map[string]*roleDiff),
		removed: make(map[string]any),
	}
	for n := range live.Roles {
		rsd.removed[n] = nil
	}
	for n, ir := range intent.Roles {
		if lr, ok := live.Roles[n]; ok {
			if rd := roleDifference(lr, ir); rd != nil {
				rsd.changed[n] = rd
			}
			delete(rsd.removed, n)
		} else {
			rsd.added[n] = nil
		}
	}
	return rsd
}

type userDiff struct {
	rolesAdded   map[string]any
	rolesRemoved map[string]any
}

type usersDiff struct {
	added   map[string]any
	changed map[string]*userDiff
	removed map[string]any
}

func userDifference(live, intent *config.User) *userDiff {
	ud := &userDiff{
		rolesAdded:   make(map[string]any),
		rolesRemoved: make(map[string]any),
	}
	for _, r := range live.Roles {
		ud.rolesRemoved[r] = nil
	}
	for _, ir := range intent.Roles {
		if slices.Contains(live.Roles, ir) {
			delete(ud.rolesRemoved, ir)
		} else {
			ud.rolesAdded[ir] = nil
		}
	}
	if len(ud.rolesAdded) == 0 && len(ud.rolesRemoved) == 0 {
		return nil
	}
	return ud
}

func usersDifference(live, intent *config.Auth) *usersDiff {
	usd := &usersDiff{
		added:   make(map[string]any),
		changed: make(map[string]*userDiff),
		removed: make(map[string]any),
	}
	for n := range live.Users {
		usd.removed[n] = nil
	}
	for n, iu := range intent.Users {
		if lu, ok := live.Users[n]; ok {
			if ud := userDifference(lu, iu); ud != nil {
				usd.changed[n] = ud
			}
			delete(usd.removed, n)
		} else {
			usd.added[n] = nil
		}
	}
	return usd
}

func grantPermission(ctx context.Context, auth clientv3.Auth, role, key string, p *config.Permission) error {
	var rangeEnd string
	if p.Prefix {
		rangeEnd = clientv3.GetPrefixRangeEnd(key)
	}
	pt, err := p.Perm.ToProto()
	if err != nil {
		return fmt.Errorf("failed to convert permission to proto for role %q key %q: %v", role, key, err)
	}
	if _, err := auth.RoleGrantPermission(ctx, role, key, rangeEnd, pt); err != nil {
		return fmt.Errorf("failed to grant permission for role %q key %q: %v", role, key, err)
	}
	return nil
}

func revokePermission(ctx context.Context, auth clientv3.Auth, role, key string, p *config.Permission) error {
	var rangeEnd string
	if p.Prefix {
		rangeEnd = clientv3.GetPrefixRangeEnd(key)
	}
	if _, err := auth.RoleRevokePermission(ctx, role, key, rangeEnd); err != nil {
		return fmt.Errorf("failed to revoke permission for role %q key %q: %v", role, key, err)
	}
	return nil
}

func Apply(ctx context.Context, auth clientv3.Auth, live, intent *config.Auth, syncPasswords bool) error {
	rsd := rolesDifference(live, intent)
	usd := usersDifference(live, intent)
	// Add new roles
	for n := range rsd.added {
		r := intent.Roles[n]
		if _, err := auth.RoleAdd(ctx, n); err != nil {
			return fmt.Errorf("failed to create new role %q: %v", n, err)
		}
		for k, p := range r.Permissions {
			if err := grantPermission(ctx, auth, n, k, p); err != nil {
				return err
			}
		}
	}
	// Add new permission grants to existing roles and modify existing permission grants
	for n, rd := range rsd.changed {
		r := intent.Roles[n]
		for k := range rd.permsAdded {
			if err := grantPermission(ctx, auth, n, k, r.Permissions[k]); err != nil {
				return err
			}
		}
		for k := range rd.permsChanged {
			if err := grantPermission(ctx, auth, n, k, r.Permissions[k]); err != nil {
				return err
			}
		}
	}
	// Add new users
	for n := range usd.added {
		u := intent.Users[n]
		if _, err := auth.UserAdd(ctx, n, u.Password); err != nil {
			return fmt.Errorf("failed to create new user %q: %v", n, err)
		}
		for _, r := range u.Roles {
			if _, err := auth.UserGrantRole(ctx, n, r); err != nil {
				return fmt.Errorf("failed to grant role %q to user %q: %v", r, n, err)
			}
		}
	}
	// Add new role grants to existing users
	for n, ud := range usd.changed {
		for r := range ud.rolesAdded {
			if _, err := auth.UserGrantRole(ctx, n, r); err != nil {
				return fmt.Errorf("failed to grant role %q to user %q: %v", r, n, err)
			}
		}
	}
	// Remove users
	for n := range usd.removed {
		if _, err := auth.UserDelete(ctx, n); err != nil {
			return fmt.Errorf("failed to remove user %q: %v", n, err)
		}
	}
	// Remove role grants from existing users
	for n, ud := range usd.changed {
		for r := range ud.rolesRemoved {
			if _, err := auth.UserRevokeRole(ctx, n, r); err != nil {
				return fmt.Errorf("failed to revoke role %q from user %q: %v", r, n, err)
			}
		}
	}
	// Remove roles (which can no longer be referenced by any user)
	for n := range rsd.removed {
		if _, err := auth.RoleDelete(ctx, n); err != nil {
			return err
		}
	}
	// Remove permission grants from existing roles and clean up conflicting permission grants
	for n, rd := range rsd.changed {
		r := live.Roles[n]
		for k := range rd.permsRemoved {
			if err := revokePermission(ctx, auth, n, k, r.Permissions[k]); err != nil {
				return err
			}
		}
		for k, revoke := range rd.permsChanged {
			if !revoke {
				continue
			}
			if err := revokePermission(ctx, auth, n, k, r.Permissions[k]); err != nil {
				return err
			}
		}
	}
	if syncPasswords {
		for name, user := range intent.Users {
			if _, err := auth.UserChangePassword(ctx, name, user.Password); err != nil {
				return fmt.Errorf("failed to sync password for user %q: %v", name, err)
			}
		}
	}
	return nil
}
