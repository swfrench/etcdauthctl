package ops_test

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
	"wikimedia.org/operations/software/etcdauthctl/internal/ops"
)

type fakeAuthClient struct {
	clientv3.Auth           // Note: only a narrow subset of methods are implemented
	roleAddErr              error
	roleDeleteErr           error
	userAddErr              error
	userDeleteErr           error
	userChangePasswordErr   error
	userGrantRoleErr        error
	userRevokeRoleErr       error
	roleGrantPermissionErr  error
	roleRevokePermissionErr error
	log                     []string
}

func (a *fakeAuthClient) RoleAdd(_ context.Context, name string) (*clientv3.AuthRoleAddResponse, error) {
	a.log = append(a.log, fmt.Sprintf("RoleAdd(%q)", name))
	if a.roleAddErr != nil {
		return nil, a.roleAddErr
	}
	return nil, nil
}

func (a *fakeAuthClient) RoleDelete(_ context.Context, name string) (*clientv3.AuthRoleDeleteResponse, error) {
	a.log = append(a.log, fmt.Sprintf("RoleDelete(%q)", name))
	if a.roleDeleteErr != nil {
		return nil, a.roleDeleteErr
	}
	return nil, nil
}

func (a *fakeAuthClient) UserAdd(_ context.Context, name, password string) (*clientv3.AuthUserAddResponse, error) {
	a.log = append(a.log, fmt.Sprintf("UserAdd(%q, %q)", name, password))
	if a.userAddErr != nil {
		return nil, a.userAddErr
	}
	return nil, nil
}

func (a *fakeAuthClient) UserDelete(_ context.Context, name string) (*clientv3.AuthUserDeleteResponse, error) {
	a.log = append(a.log, fmt.Sprintf("UserDelete(%q)", name))
	if a.userDeleteErr != nil {
		return nil, a.userDeleteErr
	}
	return nil, nil
}

func (a *fakeAuthClient) UserChangePassword(_ context.Context, name, password string) (*clientv3.AuthUserChangePasswordResponse, error) {
	a.log = append(a.log, fmt.Sprintf("UserChangePassword(%q, %q)", name, password))
	if a.userChangePasswordErr != nil {
		return nil, a.userChangePasswordErr
	}
	return nil, nil
}

func (a *fakeAuthClient) UserGrantRole(_ context.Context, name, role string) (*clientv3.AuthUserGrantRoleResponse, error) {
	a.log = append(a.log, fmt.Sprintf("UserGrantRole(%q, %q)", name, role))
	if a.userGrantRoleErr != nil {
		return nil, a.userGrantRoleErr
	}
	return nil, nil
}

func (a *fakeAuthClient) UserRevokeRole(_ context.Context, name, role string) (*clientv3.AuthUserRevokeRoleResponse, error) {
	a.log = append(a.log, fmt.Sprintf("UserRevokeRole(%q, %q)", name, role))
	if a.userRevokeRoleErr != nil {
		return nil, a.userRevokeRoleErr
	}
	return nil, nil
}

func (a *fakeAuthClient) RoleGrantPermission(_ context.Context, name, key, rangeEnd string, permType clientv3.PermissionType) (*clientv3.AuthRoleGrantPermissionResponse, error) {
	a.log = append(a.log, fmt.Sprintf("RoleGrantPermission(%q, %q, %q, %d)", name, key, rangeEnd, permType))
	if a.roleGrantPermissionErr != nil {
		return nil, a.roleGrantPermissionErr
	}
	return nil, nil
}

func (a *fakeAuthClient) RoleRevokePermission(_ context.Context, name, key, rangeEnd string) (*clientv3.AuthRoleRevokePermissionResponse, error) {
	a.log = append(a.log, fmt.Sprintf("RoleRevokePermission(%q, %q, %q)", name, key, rangeEnd))
	if a.roleRevokePermissionErr != nil {
		return nil, a.roleRevokePermissionErr
	}
	return nil, nil
}

func TestApply(t *testing.T) {
	simpleConfig := &config.Auth{
		Roles: map[string]*config.Role{
			"foo-reader": {
				Permissions: map[string]*config.Permission{
					"/foo": {Perm: config.PermRead},
				},
			},
		},
		Users: map[string]*config.User{
			"a-user": {Password: "a-password", Roles: []string{"foo-reader"}},
		},
	}
	testCases := []struct {
		name    string
		live    *config.Auth
		intent  *config.Auth
		sync    bool
		auth    *fakeAuthClient
		want    []string
		wantErr bool
	}{
		{
			name:   "from nothing",
			live:   new(config.Auth),
			intent: simpleConfig,
			auth:   new(fakeAuthClient),
			want: []string{
				`RoleAdd("foo-reader")`,
				`RoleGrantPermission("foo-reader", "/foo", "", 0)`,
				`UserAdd("a-user", "a-password")`,
				`UserGrantRole("a-user", "foo-reader")`,
			},
		},
		{
			name:   "no change",
			live:   simpleConfig,
			intent: simpleConfig,
			auth:   new(fakeAuthClient),
		},
		{
			name: "permission add",
			live: simpleConfig,
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo":  {Perm: config.PermRead},
							"/fooo": {Perm: config.PermRead},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"foo-reader"}},
				},
			},
			auth: new(fakeAuthClient),
			want: []string{
				`RoleGrantPermission("foo-reader", "/fooo", "", 0)`,
			},
		},
		{
			name: "permission remove",
			live: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo":  {Perm: config.PermRead},
							"/fooo": {Perm: config.PermRead},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"foo-reader"}},
				},
			},
			intent: simpleConfig,
			auth:   new(fakeAuthClient),
			want: []string{
				`RoleRevokePermission("foo-reader", "/fooo", "")`,
			},
		},
		{
			name: "permission change",
			live: &config.Auth{
				Roles: map[string]*config.Role{
					"fooer": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermRead},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"fooer"}},
				},
			},
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"fooer": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermReadWrite},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"fooer"}},
				},
			},
			auth: new(fakeAuthClient),
			want: []string{
				`RoleGrantPermission("fooer", "/foo", "", 2)`,
			},
		},
		{
			// A prefix change is special, in that it's ultimately a new
			// permission entirely, and must make before break.
			name: "make before break (prefix change)",
			live: simpleConfig,
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermRead, Prefix: true},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"foo-reader"}},
				},
			},
			auth: new(fakeAuthClient),
			want: []string{
				`RoleGrantPermission("foo-reader", "/foo", "/fop", 0)`,
				`RoleRevokePermission("foo-reader", "/foo", "")`,
			},
		},
		{
			name: "make before break (roles)",
			live: simpleConfig,
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader-writer": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermReadWrite},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"foo-reader-writer"}},
				},
			},
			auth: new(fakeAuthClient),
			want: []string{
				`RoleAdd("foo-reader-writer")`,
				`RoleGrantPermission("foo-reader-writer", "/foo", "", 2)`,
				`UserGrantRole("a-user", "foo-reader-writer")`,
				`UserRevokeRole("a-user", "foo-reader")`,
				`RoleDelete("foo-reader")`,
			},
		},
		{
			name: "user add and remove",
			live: simpleConfig,
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermRead},
						},
					},
				},
				Users: map[string]*config.User{
					"b-user": {Password: "b-password", Roles: []string{"foo-reader"}},
				},
			},
			auth: new(fakeAuthClient),
			want: []string{
				`UserAdd("b-user", "b-password")`,
				`UserGrantRole("b-user", "foo-reader")`,
				`UserDelete("a-user")`,
			},
		},
		{
			name:   "sync",
			live:   simpleConfig,
			intent: simpleConfig,
			auth:   new(fakeAuthClient),
			sync:   true,
			want:   []string{`UserChangePassword("a-user", "a-password")`},
		},
		// Error cases follow
		{
			name:    "role add error",
			live:    new(config.Auth),
			intent:  simpleConfig,
			auth:    &fakeAuthClient{roleAddErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name:    "role grant perm error",
			live:    new(config.Auth),
			intent:  simpleConfig,
			auth:    &fakeAuthClient{roleGrantPermissionErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name: "role revoke perm error",
			live: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo":  {Perm: config.PermRead},
							"/fooo": {Perm: config.PermRead},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{"foo-reader"}},
				},
			},
			intent:  simpleConfig,
			auth:    &fakeAuthClient{roleRevokePermissionErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name:    "role delete error",
			live:    simpleConfig,
			intent:  new(config.Auth),
			auth:    &fakeAuthClient{roleDeleteErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name:    "user add error",
			live:    new(config.Auth),
			intent:  simpleConfig,
			auth:    &fakeAuthClient{userAddErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name:    "user grant role error",
			live:    new(config.Auth),
			intent:  simpleConfig,
			auth:    &fakeAuthClient{userGrantRoleErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name:    "user delete error",
			live:    simpleConfig,
			intent:  new(config.Auth),
			auth:    &fakeAuthClient{userDeleteErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name: "user role revoke error",
			live: simpleConfig,
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {Perm: config.PermRead},
						},
					},
				},
				Users: map[string]*config.User{
					"a-user": {Password: "a-password", Roles: []string{}},
				},
			},
			auth:    &fakeAuthClient{userRevokeRoleErr: errors.New("honk")},
			wantErr: true,
		},
		{
			name:    "sync error",
			live:    simpleConfig,
			intent:  simpleConfig,
			auth:    &fakeAuthClient{userChangePasswordErr: errors.New("honk")},
			sync:    true,
			wantErr: true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := ops.Apply(context.Background(), tc.auth, tc.live, tc.intent, tc.sync)
			if gotErr := err != nil; gotErr != tc.wantErr {
				t.Fatalf("Apply() returned unexpected error: gotErr: %t wantErr: %t (got: %v)", gotErr, tc.wantErr, err)
			}
			if tc.wantErr {
				return
			}
			if diff := cmp.Diff(tc.want, tc.auth.log); diff != "" {
				t.Errorf("Apply() produced an incorrect op sequence (+got,-want):\n%s", diff)
			}
		})
	}
}
