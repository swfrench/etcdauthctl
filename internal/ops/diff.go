package ops

import (
	"fmt"
	"io"

	"github.com/pmezard/go-difflib/difflib"
	"gopkg.in/yaml.v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
)

// Diff computes the difference between the live and intent auth configuration
// represented as YAML, emitting the diff to the provided Writer and returning
// true in the presence of a diff.
func Diff(live, intent *config.Auth, contextLines int, w io.Writer) (bool, error) {
	yi, err := yaml.Marshal(intent.Redacted())
	if err != nil {
		return false, fmt.Errorf("failed to marshal redacted auth intent: %v", err)
	}
	yl, err := yaml.Marshal(live.Redacted())
	if err != nil {
		return false, fmt.Errorf("failed to marshal live auth configuration: %v", err)
	}
	diff, err := difflib.GetUnifiedDiffString(difflib.UnifiedDiff{
		A:        difflib.SplitLines(string(yl)),
		B:        difflib.SplitLines(string(yi)),
		FromFile: "Live",
		ToFile:   "Intent",
		Context:  contextLines,
	})
	if err != nil {
		return false, fmt.Errorf("failed to compute auth configuration diff: %v", err)
	}
	if diff == "" {
		fmt.Fprintf(w, "Live configuration and intent are identical\n")
		return false, nil
	}
	fmt.Fprintf(w, "Live configuration and intent differ:\n===\n%s===\n", diff)
	return true, nil
}
