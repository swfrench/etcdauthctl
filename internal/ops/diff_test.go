package ops_test

import (
	"bytes"
	"regexp"
	"slices"
	"strings"
	"testing"

	"wikimedia.org/operations/software/etcdauthctl/internal/config"
	"wikimedia.org/operations/software/etcdauthctl/internal/ops"
)

func TestDiff(t *testing.T) {
	testCases := []struct {
		name     string
		live     *config.Auth
		intent   *config.Auth
		want     bool
		patterns []string
	}{
		{
			name: "no diff",
			live: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {
								Perm: config.PermRead,
							},
						},
					},
				},
				Users: map[string]*config.User{
					"foo-user": {
						Roles: []string{"foo-reader"},
					},
				},
			},
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {
								Perm: config.PermRead,
							},
						},
					},
				},
				Users: map[string]*config.User{
					"foo-user": {
						Roles: []string{"foo-reader"},
					},
				},
			},
			patterns: []string{"^Live configuration and intent are identical"},
		},
		{
			name: "diff",
			live: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-reader": {
						Permissions: map[string]*config.Permission{
							"/foo": {
								Perm: config.PermRead,
							},
						},
					},
				},
				Users: map[string]*config.User{
					"foo-user": {
						Roles: []string{"foo-reader"},
					},
				},
			},
			intent: &config.Auth{
				Roles: map[string]*config.Role{
					"foo-writer": {
						Permissions: map[string]*config.Permission{
							"/foo": {
								Perm: config.PermWrite,
							},
						},
					},
				},
				Users: map[string]*config.User{
					"foo-user": {
						Roles: []string{"foo-writer"},
					},
				},
			},
			want: true,
			patterns: []string{
				"^Live configuration and intent differ",
				"[*]{8}", // redacted password
				// Defined roles change
				"^-[ ]+foo-reader:",
				"^[+][ ]+foo-writer:",
				// Granted roles change
				"^-[ ]+- foo-reader",
				"^[+][ ]+- foo-writer",
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			buff := new(bytes.Buffer)
			got, err := ops.Diff(tc.live, tc.intent, 5, buff)
			if err != nil {
				t.Fatalf("Diff() returned unexpected error: %v", err)
			}
			if got != tc.want {
				t.Errorf("Diff() returned incorrect status: got: %t want: %t", got, tc.want)
			}
			report := buff.String()
			lines := strings.Split(report, "\n")
			for _, p := range tc.patterns {
				re := regexp.MustCompile(p)
				if !slices.ContainsFunc(lines, func(l string) bool { return re.MatchString(l) }) {
					t.Errorf("Diff() reported incorrect content: got:\n%s\nwant: one or more lines match %q", report, p)
				}
			}
		})
	}
}
