package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/google/subcommands"
	clientv3 "go.etcd.io/etcd/client/v3"
	"wikimedia.org/operations/software/etcdauthctl/internal/commands"
	"wikimedia.org/operations/software/etcdauthctl/internal/config"
)

var (
	clientConfig = flag.String("config", "", "Path to the client configuration containing etcd endpoints, credentials, etc.")
	timeout      = flag.Duration("timeout", time.Minute, "Overall timeout on command execution.")
)

type clientWrapper struct {
	subcommands.Command
}

func loadPEM(filename string) (*x509.CertPool, error) {
	bs, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to load CA certs file: %v", err)
	}
	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(bs) {
		return nil, fmt.Errorf("failed to load PEM content from %q", filename)
	}
	return pool, nil
}

func (w *clientWrapper) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if *clientConfig == "" {
		slog.Error("No client config provided")
		return subcommands.ExitFailure
	}
	cc, err := config.LoadClient(*clientConfig)
	if err != nil {
		slog.Error("Failed to load client config", "error", err)
		return subcommands.ExitFailure
	}
	var tc *tls.Config
	if cc.CACert != "" {
		p, err := loadPEM(cc.CACert)
		if err != nil {
			slog.Error("Failed to load CA certificates file", err)
			return subcommands.ExitFailure
		}
		tc = &tls.Config{RootCAs: p}
	}
	deadline, _ := ctx.Deadline()
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   cc.Endpoints,
		Username:    cc.Username,
		Password:    cc.Password,
		TLS:         tc,
		DialTimeout: time.Until(deadline),
	})
	if err != nil {
		slog.Error("Failed to create etcd client", "error", err)
		return subcommands.ExitFailure
	}
	return w.Command.Execute(ctx, f, clientv3.NewAuth(cli))
}

func withClient(s subcommands.Command) subcommands.Command {
	return &clientWrapper{Command: s}
}

func main() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")
	subcommands.Register(withClient(commands.ShowCommand()), "")
	subcommands.Register(withClient(commands.DiffCommand()), "")
	subcommands.Register(withClient(commands.ApplyCommand()), "")
	flag.Parse()
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, nil)))
	ctx, cancel := context.WithTimeout(context.Background(), *timeout)
	defer cancel()
	os.Exit(int(subcommands.Execute(ctx)))
}
